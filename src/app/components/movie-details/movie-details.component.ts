import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  movie: any;



  constructor(private service: MoviesService,
              private activatedRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      qparams => {
        const q = qparams.imdbId;
        this.service.getMovieDetails(q)
  .subscribe(resp => this.movie = resp);
      }
    // data response...
    // 2 error in the stream
    // 3 normal subscription
    );
}
goBack() {
    window.history.back();
  }
  }
