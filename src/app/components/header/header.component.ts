import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private route: Router) { }
  queryTerm: string;
  ngOnInit() {
  }

  submitHandler(evt) {
    evt.preventDefault();
    this.route.navigate(['/movies'], {queryParams: {param: this.queryTerm}});
  }
}
